# Pruned Tree by Lucas Castro

Solution to the assessment described here:

https://docs.google.com/document/d/1kS-e5fS7pqAeW0OzjI-JElMCuqWBCZtwgk_eFHssUOc/edit


Please set the `SERVICE_URL` with the upstream URL before run the webapp

```
bundle install
export SERVICE_URL='https://kf6xwyykee.execute-api.us-east-1.amazonaws.com/production/tree'
rails s
```

Open the browser:
```
http://localhost:3000/tree/input?indicator_ids[]=31&indicator_ids[]=32&indicator_ids[]=1
```

And to run the tests:

```
rspec
```
