require 'rails_helper'

RSpec.describe TreeController, type: :controller do
  before do
    @tree_json         = File.open('spec/fixtures/tree.json', 'r').read
    @error_json        = { error: "Can't find that tree" }.to_json
    @tree_name         = 'input'
    @tree              = JSON.parse(@tree_json, :symbolize_names => true)

    ENV['SERVICE_URL'] = 'https://localhost:5000/tree'
    @expected_uri      = "#{ENV['SERVICE_URL']}/#{@tree_name}"

    stub_request(:get, @expected_uri).to_return(body: @tree_json)
  end

  describe "GET #show" do
    context "with an existing tree" do
      it "returns 200 status code" do
        get :show, params: { name: 'input' }
        expect(response).to have_http_status(:success)
      end

      context "without indicator_id's" do
        it "returns an empty tree" do
          get :show, params: { name: 'input' }
          expect(response.body).to match [].to_json
        end
      end

      context "with indicator_id's" do
        it "returns the pruned tree" do
          get :show, params: { name: 'input', indicator_ids: [1] }

          pruner = Pruner.new(@tree)
          pruned = pruner.prune([1])
          expect(response.body).to eq pruned.to_json
        end
      end
    end

    context "to a not existing tree" do
      it "returns 404 status code" do
        stub_request(:get, "#{ENV['SERVICE_URL']}/notree").to_return(body: @error_json)
        get :show, params: { name: 'notree', indicator_ids: [1] }
        expect(response.status).to eq 404
      end
    end

    context "with error on access the upstream service" do
      it "returns 500 status code" do
        stub_request(:get, @expected_uri).to_return(status: 500)
        get :show, params: { name: 'input', indicator_ids: [1] }
        expect(response.status).to eq 500
      end
    end
  end
end
