require 'rails_helper'

RSpec.describe Broker, :type => :model do
  before do
    @tree_json  = File.open('spec/fixtures/tree.json', 'r').read
    @error_json = { error: "Can't find that tree" }.to_json
    @tree_name  = 'input'
    @broker     = Broker.new('https://localhost:5000/tree')
  end

  context "#get_tree" do
    context "with successful response" do
      before do
        @expected_uri = "#{@broker.base_url}/#{@tree_name}"
        stub_request(:get, @expected_uri).to_return(body: @tree_json)
      end

      it "requests the tree" do
        @broker.get_tree(@tree_name)
        assert_requested :get, @expected_uri, times: 1
      end

      it "returns the response" do
        response = @broker.get_tree(@tree_name)
        expect(response.class).to eq Response
      end

      it "sets the response code = 200" do
        response = @broker.get_tree(@tree_name)
        expect(response.code).to eq 200
      end
    end

    context "with tree not found error" do
      before do
        @tree_name = "notree"
        @uri       = "#{@broker.base_url}/#{@tree_name}"
        stub_request(:get, @uri).to_return(body: @error_json)
      end

      it "sets the response code = 404" do
        response = @broker.get_tree(@tree_name)
        expect(response.code).to eq 404
      end
    end

    context "with error the upstream service" do
      before do
        @broker.delay_to_retry = 0 # Just to speed up the tests.

        @uri = "#{@broker.base_url}/tree"
        stub_request(:get, @uri).to_return(status: 500)
      end

      it "sets the response with 500 code" do
        response = @broker.get_tree('tree')
        expect(response.code).to eq 500
      end

      it "tries up to 3 times" do
        @broker.get_tree('tree')
        assert_requested :get, @uri, times: 3
      end

      it "delays between the attempts" do
        allow(@broker).to receive(:delay)
        @broker.get_tree('tree')
        expect(@broker).to have_received(:delay).exactly(3).times
      end

      it "tries up to get a tree not found error" do
        stub_request(:get, @uri).
          to_return(status: 500).times(1).then.
          to_return(body: @error_json)

        @broker.get_tree('tree')
        assert_requested :get, @uri, times: 2
      end

      it "tries up to get a successful response" do
        stub_request(:get, @uri).
          to_return(status: 500).times(1).then.
          to_return(body: @tree_json)

        @broker.get_tree('tree')
        assert_requested :get, @uri, times: 2
      end
    end
  end
end
