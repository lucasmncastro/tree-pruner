require 'rails_helper'

RSpec.describe Pruner, :type => :model do
  before do
    tree_json = File.open('spec/fixtures/tree.json', 'rb').read
    @tree     = JSON.parse(tree_json, :symbolize_names => true)
    @pruner   = Pruner.new(@tree)
  end

  context '#prune' do
    it 'works with only one indicator' do
      pruned_tree = @pruner.prune([299])
      expect(pruned_tree).to match([{
        id: 1,
        name: 'Urban Extent',
        sub_themes: [{
          categories: [{
            id: 1,
            indicators: [{
              id: 299,
              name: 'Total'
            }],
            name: 'Area',
            unit: '(sq. km.)'
          }],
          id: 1,
          name: 'Administrative'
        }]
      }])
    end

    it 'works with multiple indicators of the same category' do
      pruned_tree = @pruner.prune([1, 2])
      expect(pruned_tree).to match([{
        id: 2,
        name: 'Demographics',
        sub_themes: [{
          categories: [{
            indicators: [{
              id: 1,
              name: 'total'
            }, {
              id: 2,
              name: 'male'
            }],
            id: 11,
            name: 'Crude death rate',
            unit: '(deaths per 1000 people)'
          }],
          id: 4,
          name: 'Births and Deaths'
        }]
      }])
    end

    it 'works with multiple indicators of different categories' do
      pruned_tree = @pruner.prune([308, 2])
      expect(pruned_tree).to match([{
        id: 2,
        name: 'Demographics',
        sub_themes: [{
          categories: [{
            indicators: [{
              id: 308,
              name: 'Total'
            }],
            id: 10,
            name: 'Crude birth rate',
            unit: '(live births per 1000 people)'
          }, {
            indicators: [{
              id: 2,
              name: 'male'
            }],
            id: 11,
            name: 'Crude death rate',
            unit: '(deaths per 1000 people)'
          }],
          id: 4,
          name: 'Births and Deaths'
        }]
      }])
    end

    it 'works with multiple indicators of different sub-themes' do
      pruned_tree = @pruner.prune([308, 9])
      expect(pruned_tree).to match([{
        id: 2,
        name: 'Demographics',
        sub_themes: [{
          categories: [{
            id: 10,
            indicators: [{
              id: 308,
              name: 'Total'
            }],
            name: 'Crude birth rate',
            unit: '(live births per 1000 people)'
          }],
          id: 4,
          name: 'Births and Deaths'
        }, {
          categories: [{
            id: 13,
            indicators: [{
              id: 9,
              name: '0-4 years'
            }],
            name: 'Gender ratio',
            unit: '(percent)'
          }],
          id: 5,
          name: 'Age and Sex'
        }]
      }])
    end

    it 'works with multiple indicators of different themes' do
      pruned_tree = @pruner.prune([308, 11])
      expect(pruned_tree).to match([{
        id: 2,
        name: 'Demographics',
        sub_themes: [{
          categories: [{
            id: 10,
            indicators: [{
              id: 308,
              name: 'Total'
            }],
            name: 'Crude birth rate',
            unit: '(live births per 1000 people)'
          }],
          id: 4,
          name: 'Births and Deaths'
        }]
      }, {
        id: 3,
        name: 'Jobs',
        sub_themes: [{
          categories: [{
            id: 16,
            indicators: [{
              id: 11,
              name: 'Female'
            }],
            name: 'Working population, 15+ years',
            unit: '(percent)'
          }],
          id: 7,
          name: 'Labor force'
        }]
      }])
    end
  end
end
