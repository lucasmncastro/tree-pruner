class Response
  attr_accessor :http_response, :code

  def initialize(http_response)
    @http_response = http_response
    @code = http_response.code
    @code = 404 if body_with_error?
  end

  def body_with_error?
    (parsed_body.is_a? Hash and parsed_body[:error])
  end

  def successful?
    @code == 200
  end

  def error?
    @code == 404
  end

  def failed?
    @code == 500
  end

  def parsed_body
    return unless successful?
    JSON.parse(http_response.body, symbolize_names: true)
  end
end
