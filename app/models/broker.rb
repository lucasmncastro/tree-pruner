require 'httparty'

class Broker
  attr_accessor :base_url, :tree_name, :delay_to_retry

  def initialize(base_url)
    @base_url = base_url
    
    @delay_to_retry      = 1 # in seconds
    @maximum_of_attempts = 3
  end

  def get_tree(tree_name)
    response = nil

    @maximum_of_attempts.times do
      response = call("#{base_url}/#{tree_name}")
      return response unless response.failed?
      delay
    end

    response
  end

  def delay
    sleep(@delay_to_retry)
  end

  def call(url)
    http_response = HTTParty.get(url)
    Response.new(http_response)
  end
end
