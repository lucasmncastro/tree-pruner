class Pruner
  attr_reader :tree

  def initialize(tree)
    @tree = tree
  end

  def prune(indicator_ids)
    tree.select do |theme|
      sub_themes = theme[:sub_themes].select do |sub_theme|
        categories = sub_theme[:categories].select do |category|
          indicators = category[:indicators].select do |indicator|
            indicator_ids.include? indicator[:id]
          end

          category[:indicators] = indicators
          indicators.any?
        end

        sub_theme[:categories] = categories
        categories.any?
      end

      theme[:sub_themes] = sub_themes
      sub_themes.any?
    end
  end
end
