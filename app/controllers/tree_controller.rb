class TreeController < ApplicationController
  def show
    service_url = ENV['SERVICE_URL'] 
    broker      = Broker.new(service_url)
    response    = broker.get_tree(params[:name])

    if response.successful?
      indicator_ids = params[:indicator_ids].to_a.map(&:to_i)

      result = Pruner.new(response.parsed_body).prune(indicator_ids)
      render json: result.to_json, status: 200
    else
      render json: {error: true}.to_json, status: response.code
    end
  end
end
